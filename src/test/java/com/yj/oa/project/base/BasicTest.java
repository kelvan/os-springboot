package com.yj.oa.project.base;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * Created by yanzi on 2017/9/19.
 */
//这是JUnit的注解，通过这个注解SpringRunner类提供Spring测试上下文。
@RunWith(SpringRunner.class)
//springBoot测试注解
@SpringBootTest
//测试类事务回滚
@Transactional
public class BasicTest {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

}

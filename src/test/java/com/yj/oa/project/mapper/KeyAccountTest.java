package com.yj.oa.project.mapper;

import com.yj.oa.project.base.BasicTest;
import org.junit.Test;

import javax.annotation.Resource;

/**
 Created by kelvan.cai on 2019/7/10 0010.
 */
public class KeyAccountTest extends BasicTest{
    @Resource
    private KeyAccountMapper keyAccountMapper;

    @Test
    public void selectKeyAccount(){
        System.out.println("keyAccount:"+ keyAccountMapper.selectKeyAccount());
    }
}

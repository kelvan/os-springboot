package com.yj.oa.project.mapper;

import com.yj.oa.project.base.BasicTest;
import com.yj.oa.project.po.User;
import org.junit.Test;

import javax.annotation.Resource;

/**
  Created by kelvan.cai on 2019/6/26 0026.
 */
public class UserMapperTest extends BasicTest{
    @Resource
    private UserMapper userMapper;

    @Test
    public void selectByPrimaryKey(){
        User uid = userMapper.selectByPrimaryKey("2019061904");
        System.out.println(uid);
    }
}

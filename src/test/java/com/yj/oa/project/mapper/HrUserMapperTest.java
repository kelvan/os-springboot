package com.yj.oa.project.mapper;

import com.yj.oa.project.base.BasicTest;
import com.yj.oa.project.po.HrUser;
import org.junit.Test;

import javax.annotation.Resource;

/**
  Created by kelvan.cai on 2019/7/2 0002.
 */
public class HrUserMapperTest extends BasicTest{
    @Resource
    private HrUserMapper hrUserMapper;

    @Test
    public void getHrUserByName(){
        HrUser user = hrUserMapper.getHrUserByName("kelvan.cai");
        System.out.println("上级领导名字："+user.getSupervisorChnName());
        System.out.println("上级领导邮箱："+user.getSupervisorMail());
    }
}

package com.yj.oa;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.repository.Deployment;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.InputStream;
import java.util.zip.ZipInputStream;

/**
 * @author kelvan.cai
 * @描述 启动类
 *
 * @date 2018/9/23 10:53
 */
@SpringBootApplication
@MapperScan("com.yj.oa.project.mapper")
//开启定时任务
@EnableScheduling
//开启缓存
@EnableCaching
public class OaApplication {

    public static void main(String[] args)
    {
        SpringApplication.run(OaApplication.class, args);
        String fozu =
                        "                   _ooOoo_"+"\n"+
                        "                  o8888888o"+"\n"+
                        "                  88\" . \"88"+"\n"+
                        "                  (| -_- |)"+"\n"+
                        "                  O\\  =  /O"+"\n"+
                        "               ____/`---'\\____"+"\n"+
                        "             .'  \\\\|     |//  `."+"\n"+
                        "            /  \\\\|||  :  |||//  \\"+"\n"+
                        "           /  _||||| -:- |||||-  \\"+"\n"+
                        "           |   | \\\\\\  -  /// |   |"+"\n"+
                        "           | \\_|  ''\\---/''  |   |"+"\n"+
                        "           \\  .-\\__  `-`  ___/-. /"+"\n"+
                        "         ___`. .'  /--.--\\  `. . __"+"\n"+
                        "      .\"\" '<  `.___\\_<|>_/___.'  >'\"\"."+"\n"+
                        "     | | :  `- \\`.;`\\ _ /`;.`/ - ` : | |"+"\n"+
                        "     \\  \\ `-.   \\_ __\\ /__ _/   .-` /  /"+"\n"+
                        "======`-.____`-.___\\_____/___.-`____.-'======"+"\n"+
                        "                   `=---='"+"\n"+
                        "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"+"\n"+
                        ""
//                        "              佛祖保佑     永无BUG"
                ;
//        System.out.println(fozu);
        System.out.println("(♥◠‿◠)ﾉﾞ  OA-SYSTEM STARTING SUCCESS   ლ(´ڡ`ლ)ﾞ  \n");
        //部署工作流程
       // ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();


        //InputStream inputStream = new OaApplication().getClass().getClassLoader().getResourceAsStream("processes/leave.zip");
        //ZipInputStream zipInputStream = new ZipInputStream(inputStream);
//
////
//        Deployment deployment = defaultProcessEngine.getRepositoryService()
//                //部署对象
//                .createDeployment()
//                //部署名称
//                .name("会议室申请流程")
//                //加载文件，一次只能加载一个 可以加载zip
////                .addClasspathResource("processes/leave.bpmn")
////                .addClasspathResource("processes/leave.png")
//                .addClasspathResource("processes/apply.bpmn")
//                .addClasspathResource("processes/apply.png")
//        //.addZipInputStream(zipInputStream)
//                .deploy();
//        System.out.println("$$$$$ 部署流程");
//        System.out.println("部署ID:" + deployment.getId());
//        System.out.println("部署名称：" + deployment.getName());

//        initApplyProcess();
//        initLeaveProcess();

    }

    private static void initLeaveProcess(){
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        InputStream inputStream = new OaApplication().getClass().getClassLoader().getResourceAsStream("processes/leave.zip");
        ZipInputStream zipInputStream = new ZipInputStream(inputStream);
        Deployment deployment = defaultProcessEngine.getRepositoryService()
                //部署对象
                .createDeployment()
                //部署名称
                .name("借人申请流程")
                //加载文件，一次只能加载一个 可以加载zip
                .addZipInputStream(zipInputStream)
                .deploy();
    }

    private static void initApplyProcess(){
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        InputStream inputStream = new OaApplication().getClass().getClassLoader().getResourceAsStream("processes/apply.zip");
        ZipInputStream zipInputStream = new ZipInputStream(inputStream);
        Deployment deployment = defaultProcessEngine.getRepositoryService()
                //部署对象
                .createDeployment()
                //部署名称
                .name("会议室申请流程")
                //加载文件，一次只能加载一个 可以加载zip
                .addZipInputStream(zipInputStream)
                .deploy();
    }
}

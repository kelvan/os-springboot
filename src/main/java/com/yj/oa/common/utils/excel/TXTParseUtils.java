package com.yj.oa.common.utils.excel;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 kelvan.cai on 2019/7/12 0012.
 */
public class TXTParseUtils {

    public static void txtParseUtils(String filePath){
        File file = new File(filePath);
        try {
        if(file.isFile() && file.exists()) {
            InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "utf-8");
            BufferedReader br = new BufferedReader(isr);
            String lineTxt = null;
            while ((lineTxt = br.readLine()) != null) {
                System.out.println(lineTxt);
            }
            br.close();
        } else {
            System.out.println("文件不存在!");
        }
    } catch (Exception e) {
        System.out.println("文件读取错误!");
    }
}

    public static void main(String[] args) {
        String filePath = "E:\\value.txt";
        TXTParseUtils.txtParseUtils(filePath);

    }





}

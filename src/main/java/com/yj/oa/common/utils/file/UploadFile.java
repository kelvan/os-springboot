package com.yj.oa.common.utils.file;

import com.yj.oa.common.exception.BusinessException;
import com.yj.oa.common.exception.file.FileNameLengthException;
import com.yj.oa.common.exception.file.FileSizeException;
import com.yj.oa.common.utils.DateUtils;
import com.yj.oa.common.utils.ftp.FtpUtil;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 永健
 * 上传工具类封装
 */
public class UploadFile {

    private static Logger Log = LoggerFactory.getLogger(UploadFile.class);
    //http://106.14.226.138:7777
    //http://103.120.82.188:8888/
    public static final String basepath = "http://103.120.82.188:8888/";
    /**
     * 默认大小 10M //字节
     */
    public static final long DEFAULT_MAX_SIZE = 10485760;
    /**
     * 默认的文件名最大长度
     */
    public static final int DEFAULT_FILE_NAME_LENGTH = 100;


    //------------------ 原数据------------------------

    /**
     *
     * @描述 文件上传
     *
     * @date 2018/9/19 20:41
     */
     //ftp上传
    public static String upload(MultipartFile file) throws IOException, FileSizeException, FileNameLengthException
    {

        String fileId = getFileTimeId();
        //判断文件大小
        assertAllowed(file);
        //检查文件名字长度
        checkFileNameLength(file);
        //获得文件流
        InputStream input = file.getInputStream();
        //获得文件名字
        String name = fileId + file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."),
                                                                    file.getOriginalFilename().length());
        Map<String, InputStream> fileMap = new HashMap<>();
        fileMap.put(name, input);
        //调用方法执行上传
        return FtpUtil.uploadFile(fileMap) ? name : null;
    }


    /**
     * 头像上传
     *
     * @param file 图片文件
     */
    public static String uploadUserImg(
            MultipartFile file) throws FileNameLengthException, FileSizeException, IOException
    {
        return basepath + upload(file);
    }


    /**
     *
     * @描述 下载
     *
     * @date 2018/9/19 20:41
     */
    public static ResponseEntity<byte[]> download(String fileId, String fileName) throws IOException
    {
        return FtpUtil.fileDown(fileId, fileName);
    }

    //本地下载
    public static ResponseEntity<byte[]> download2(File file,String fileName)throws IOException{
        HttpHeaders headers = new HttpHeaders();
        if (fileName.endsWith("PDF"))
            headers.setContentType(MediaType.valueOf(MediaType.APPLICATION_PDF_VALUE + ";charset=UTF-8"));
        else
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", fileName);
        //edit by sai 2017-3-14,对显示、下载的文件（图片），增加缓存功能，提高访问速度
        headers.setExpires(System.currentTimeMillis()+24*7*60*60*1000);//设置缓存24*7小时
        try {
            return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file),headers, HttpStatus.CREATED);
        } catch (IOException e) {
            throw new BusinessException(e.getMessage());
        }
    }


    /**
     *
     * @描述 文件删除
     *
     * @date 2018/9/19 23:47
     */
    public static boolean delFile(String[] ids) throws IOException
    {
        return FtpUtil.deleteFile(ids);
    }


    /**
     *
     * @描述 文件名长度校验
     *
     * @date 2018/9/19 10:55
     */
    public static void checkFileNameLength(MultipartFile file) throws FileNameLengthException
    {
        int fileNamelength = file.getOriginalFilename().length();
        if (fileNamelength > DEFAULT_FILE_NAME_LENGTH)
        {
            throw new FileNameLengthException("文件名字超长", file.getOriginalFilename(), fileNamelength,
                                              DEFAULT_FILE_NAME_LENGTH);
        }
    }


    /**
     *
     * @描述 文件大小校验
     *
     * @date 2018/9/19 10:55
     */
    //ftp上传
    public static void assertAllowed(MultipartFile file) throws FileSizeException
    {
        long size = file.getSize();
        if (DEFAULT_MAX_SIZE != -1 && size > DEFAULT_MAX_SIZE)
        {
            throw new FileSizeException("文件大小超出范围", size,
                                        DEFAULT_MAX_SIZE);
        }
    }


    /**
     *
     * @描述 生成文件id 也就是文件上传到服务器的名字
     *
     * @date 2018/9/19 13:34
     */
    public static String getFileTimeId()
    {
        return DateUtils.DateToSTr(new Date()).replace(" ", "_").replace(":", "-");
    }

    //------------------ 原数据------------------------

    //***************本地上传******************

//    /**
//     * 默认上传的地址
//     */
//    private static String defaultBaseDir = Global.getProfile();
//    /**
//     * 默认文件类型xlsx
//     */
//    public static final String IMAGE_JPG_EXTENSION = ".xlsx";//jpg
//
//    private static int counter = 0;
//
//    public static void setDefaultBaseDir(String defaultBaseDir)
//    {
//        UploadFile.defaultBaseDir = defaultBaseDir;
//    }
//
//    public static String getDefaultBaseDir()
//    {
//        return defaultBaseDir;
//    }
//
//
//    /**
//     * 以默认配置进行文件上传
//     *
//     * @param file 上传的文件
//     * @return 文件名称
//     * @throws Exception
//     */
//    public static final String upload(MultipartFile file) throws IOException
//    {
//        try
//        {
//            return upload(getDefaultBaseDir(), file, UploadFile.IMAGE_JPG_EXTENSION);
//        }
//        catch (Exception e)
//        {
//            throw new IOException(e.getMessage(), e);
//        }
//    }
//
//    /**
//     * 根据文件路径上传
//     *
//     * @param baseDir 相对应用的基目录
//     * @param file 上传的文件
//     * @return 文件名称
//     * @throws IOException
//     */
//    public static final String upload(String baseDir, MultipartFile file) throws IOException
//    {
//        try
//        {
//            return upload(baseDir, file, UploadFile.IMAGE_JPG_EXTENSION);
//        }
//        catch (Exception e)
//        {
//            throw new IOException(e.getMessage(), e);
//        }
//    }
//
//    /**
//     * 文件上传
//     *
//     * @param baseDir 相对应用的基目录
//     * @param file 上传的文件
//     * @param extension 上传文件类型
//     * @return 返回上传成功的文件名
//     * @throws FileSizeLimitExceededException 如果超出最大大小
//     * @throws FileNameLengthLimitExceededException 文件名太长
//     * @throws IOException 比如读写文件出错时
//     */
//    public static final String upload(String baseDir, MultipartFile file, String extension)
//            throws FileSizeLimitExceededException, IOException, FileNameLengthLimitExceededException
//    {
//
//        int fileNamelength = file.getOriginalFilename().length();
//        if (fileNamelength > UploadFile.DEFAULT_FILE_NAME_LENGTH)
//        {
//            throw new FileNameLengthLimitExceededException(UploadFile.DEFAULT_FILE_NAME_LENGTH);
//        }
//
//        assertAllowed(file);
//
//        String fileName = extractFilename(file, extension);
//
//        File desc = getAbsoluteFile(baseDir, baseDir + fileName);
//        file.transferTo(desc);
//        return fileName;
//    }
//
//    public static final String extractFilename(MultipartFile file, String extension)
//    {
//        String filename = file.getOriginalFilename();
//        filename = DateUtils.datePath() + "/" + encodingFilename(filename) + extension;
//        return filename;
//    }
//
//    private static final File getAbsoluteFile(String uploadDir, String filename) throws IOException
//    {
//        File desc = new File(File.separator + filename);
//
//        if (!desc.getParentFile().exists())
//        {
//            desc.getParentFile().mkdirs();
//        }
//        if (!desc.exists())
//        {
//            desc.createNewFile();
//        }
//        return desc;
//    }
//
//    /**
//     * 编码文件名
//     */
//    private static final String encodingFilename(String filename)
//    {
//        filename = filename.replace("_", " ");
//        filename = Md5Utils.hash(filename + System.nanoTime() + counter++);
//        return filename;
//    }
//
//    /**
//     * 文件大小校验
//     *
//     * @param file 上传的文件
//     * @return
//     * @throws FileSizeLimitExceededException 如果超出最大大小
//     */
//    public static final void assertAllowed2(MultipartFile file) throws FileSizeLimitExceededException
//    {
//        long size = file.getSize();
//        if (DEFAULT_MAX_SIZE != -1 && size > DEFAULT_MAX_SIZE)
//        {
//            throw new FileSizeLimitExceededException(DEFAULT_MAX_SIZE / 1024 / 1024);
//        }
//    }
//
//    /**
//     * 文件大小校验
//     *
//     * @param file 上传的文件
//     * @return
//     * @throws FileSizeLimitExceededException 如果超出最大大小
//     */
//    public static final void assertAllowed(MultipartFile file) throws FileSizeLimitExceededException
//    {
//        long size = file.getSize();
//        if (DEFAULT_MAX_SIZE != -1 && size > DEFAULT_MAX_SIZE)
//        {
//            throw new FileSizeLimitExceededException(DEFAULT_MAX_SIZE / 1024 / 1024);
//        }
//    }
    //***************本地上传******************
}

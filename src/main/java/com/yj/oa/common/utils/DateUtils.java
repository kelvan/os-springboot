package com.yj.oa.common.utils;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * @author kelvan.cai
 * 日期工具类
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils{

    private static Logger log = LoggerFactory.getLogger(DateUtils.class);
    private static Calendar calendar = Calendar.getInstance();

    private static String[] parsePatterns = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};

    /**
     *
     * @描述 date to str
     *
     * @date 2018/9/15 20:49
     */
    public static String DateToSTr(Date date)
    {
        SimpleDateFormat aDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = aDate.format(date);
        return format;
    }

    public static final String parseDateToStr(final String format, final Date date)
    {
        return new SimpleDateFormat(format).format(date);
    }

    /**
     * 转换为年月日字符窜
     */
    public static String DateToSTr2(Date date)
    {
        SimpleDateFormat aDate = new SimpleDateFormat("yyyy-MM-dd");
        String format = aDate.format(date);
        return format;
    }

    /**
     * 转换为年月日字符窜
     */
    public static String DateToSTr3(Date date)
    {
        SimpleDateFormat aDate = new SimpleDateFormat("yyyyMMdd");
        String format = aDate.format(date);
        return format;
    }


    /**
     *
     * @描述: 字符窜转日期
     *
     * @params:
     * @return:
     * @date: 2018/9/29 11:28
     */
    public static Date StrToDate(String date)
    {
        SimpleDateFormat aDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parse = null;
        try
        {
            parse = aDate.parse(date);
        }
        catch (ParseException e)
        {
            log.error("日期格式化出错=[{}]", date);
        }
        return parse;
    }


    /**
     * 获取上一个月
     *
     * @param date 当前日期的上一个月
     */
    public static Date getPreMoth(Date date)
    {
        //获取上个月日期
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MONTH, -1);
        Date pre = c.getTime();
        return pre;
    }


    /**
     * 获取周几
     */
    public static String getTodayWeek()
    {
        //获取当前时间
        calendar.setTime(new Date());
        int week = calendar.get(Calendar.DAY_OF_WEEK) - 1;

        if (week < 0)
        {
            week = 7;
        }
        return weekToStr(week);
    }


    public static String weekToStr(int week)
    {
        String w = "";
        switch (week)
        {
            case 7:
                w = "日";
                break;
            case 6:
                w = "六";
                break;
            case 5:
                w = "五";
                break;
            case 4:
                w = "四";
                break;
            case 3:
                w = "三";
                break;
            case 2:
                w = "二";
                break;
            case 1:
                w = "一";
                break;
            default:
                w = "";
                break;
        }
        return w;
    }


    /**
     * 获取 时 分 秒 字符窜
     */
    public static String getTimeShort(Date date)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        String dateString = formatter.format(date);

        return dateString.equals("00:00:00") ? "12:00:00" : dateString;
    }


    /**
     * 计算时间戳的时间差
     *
     * @param start 减数
     * @param end 被减数
     *
     * @return 分钟
     */
    public static long getTimeRang(long start, long end)
    {

        long l = (end - start) / (1000 * 60);
        return l;
    }

    /**
     * 计算时间戳的时间差
     *
     * @param start 减数
     * @param end 被减数
     *
     * @return 小时
     */
    public static long getHouRang(long start, long end)
    {

        long l = (end - start) / (1000 * 60 * 60);
        return l;
    }


    /**
     * 计算两个时间之间相差的小时和分钟数
     *
     * @param nowDate
     * @param endDate
     * @return
     */
    public static String getHourRang(Date nowDate,Date endDate){
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        //long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return  hour + "小时" + min + "分钟";
    }


    /**
     * 获取当前月的天数
     */
    public static int getDayOfMonth(Date date)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int day = c.getActualMaximum(Calendar.DATE);
        return day;
    }


    /**
     * 获取个月的第一天
     */
    public static Date getFirstDayDateOfMonth(final Date date)
    {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        final int last = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.DAY_OF_MONTH, last);
        return cal.getTime();
    }


    /**
     * 获取月的最后一天
     */
    public static Date getLastDayOfMonth(final Date date)
    {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        final int last = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.DAY_OF_MONTH, last);
        return cal.getTime();
    }


    /**
     *  获取小时
     * @param date
     * @return
     */
    public static int getHours(Date date)
    {
        Calendar c=Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.HOUR_OF_DAY);
    }


    /**
     * 获取分钟
     * @param date
     * @return
     */
    public static int getMinute(Date date)
    {
        Calendar c=Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.MINUTE);
    }

    /**
     * 获取秒
     * @param date
     * @return
     */
    public static int getSecend(Date date)
    {
        Calendar c=Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.SECOND);
    }

    /**
     * 日期型字符串转化为日期 格式
     */
    public static Date parseDate(Object str)
    {
        if (str == null)
        {
            return null;
        }
        try
        {
            return parseDate(str.toString(), parsePatterns);
        }
        catch (ParseException e)
        {
            return null;
        }
    }

    /**
     * 日期路径 即年/月/日 如2018/08/08
     */
    public static final String datePath()
    {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyy/MM/dd");
    }
}

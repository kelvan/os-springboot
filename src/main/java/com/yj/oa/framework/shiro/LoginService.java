package com.yj.oa.framework.shiro;

import com.yj.oa.common.utils.ServletUtils;
import com.yj.oa.common.utils.StringUtils;
import com.yj.oa.common.utils.shiro.Encryption;
import com.yj.oa.project.po.User;
import com.yj.oa.project.service.user.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.Date;
import java.util.Hashtable;

import static com.yj.oa.project.po.VerificationCode.ValidateCodeUtil.RANDOMCODEKEY;

/**
 * @author kelvan.cai
 * 登录验证用户的还在那个号密码以及验证码 是否为空
 */
@Component
public class LoginService{
    private final static Logger logger = LoggerFactory.getLogger(LoginService.class);
    /**
     * 验证码 sessinName
     */
    private final static String VALIDATE_CODE="validateCode";

    @Autowired
    IUserService iUserService;

    /**
     *
     * @描述: 登录校验
     *
     * @date: 2018/9/29 21:47
     */
    public void checkLogin(String loginName, String pwd,String validateCode) throws Exception
    {
        HttpServletRequest request = ServletUtils.getRequest();
        HttpSession session = request.getSession();
        String code = (String) session.getAttribute(RANDOMCODEKEY);
        String inputCode = validateCode.toUpperCase();
        //初始化用户名，如果用户名包含@osmglobal.com，只取@前面的用户名
        if(loginName.contains("@osmglobal.com")){
            String u[] = loginName.split("@");
            String u1=u[0];
            loginName = u1;
        }
        String userName = loginName;//AD域认证，用户的登录UserName
        String passWord = pwd;//AD域认证，用户的登录PassWord
        String host = "10.1.3.7";//AD域IP，必须填写正确
        String domain = "@osmglobal.com";//域名后缀，例.@noker.cn.com
        String port = "389"; //端口，一般默认389
        String url = new String("ldap://" + host + ":" + port);//固定写法
        String user2 = userName.indexOf(domain) > 0 ? userName : userName
                + domain;//网上有别的方法，但是在我这儿都不好使，建议这么使用
        Hashtable env = new Hashtable();//实例化一个Env
        DirContext ctx = null;
        env.put(Context.SECURITY_AUTHENTICATION, "simple");//LDAP访问安全级别(none,simple,strong),一种模式，这么写就行
        env.put(Context.SECURITY_PRINCIPAL, user2); //用户名
        env.put(Context.SECURITY_CREDENTIALS, passWord);//密码
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");// LDAP工厂类
        env.put(Context.PROVIDER_URL, url);//Url
        boolean flag=false;
        try {
            ctx = new InitialDirContext(env);// 初始化上下文
            System.out.println("身份验证成功!");
            flag=true;
        } catch (AuthenticationException e) {
            System.out.println("身份验证失败!");
        } catch (javax.naming.CommunicationException e) {
            System.out.println("AD域连接失败!");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("身份验证未知异常!");
            e.printStackTrace();
        } finally{
            if(null!=ctx){
                try {
                    ctx.close();
                    ctx=null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if(flag){
            //验证成功
            System.out.println("域登录验证成功！");
            //1.域登录验证成功之后获取用户名保存进数据库
            //2.用户未登陆过添加数据进数据库
            //3.用户已登陆过 验证密码是否正确，错误即更新密码以及保存进数据库！
            if(iUserService.getUserByName(userName) == null){
                //域登录时给首次登陆的默认状态
                User userAD = new User();
//                if(userName.contains("@osmglobal.com")){//1如果用户名包含@osmglobal.com，则返回@之前的登录名
//                    String u[] = userName.split("@");
//                    String u1=u[0];
//                    userAD.setLoginName(u1);
//                    userAD.setName(u1);
//                }else {
//                    userAD.setLoginName(userName);
//                    userAD.setName(userName);
//                }
                userAD.setLoginName(userName);
                userAD.setName(userName);
                userAD.setCreateTime(new Date());
                userAD.setDept(4);
                userAD.setPosition(6);
                userAD.setStatus(0);//正常状态
                userAD.setRole_ID(3);//普通员工
                userAD.setIsdel(0);
                userAD.setUid("");//随意写，不影响最后生成的用户编号,但必须要set,否则验证不通过
                userAD.setAvatar("/img/osm.jpg");//默认用户头像
                //String md5Pwd = Encryption.getMD5(passWord, userAD.getUid()).toString();
                userAD.setPwd(passWord);
                //初始化邮件
//                if(userName.contains("@osmglobal.com")){//1如果用户名包含@osmglobal.com，则返回登录名
//                    userAD.setEmail(userName);
//                }else{
                    userAD.setEmail(userName+domain);//2如果不包含则返回登录名+@osmglobal.com
//                }
                int insertUser = iUserService.insertSelective(userAD);
                if(insertUser>0){
                    System.out.println("添加用户成功！");
                }else {
                    System.out.println("添加用户失败！");
                }
            }else{
                //拿到数据库密码，对比用户输入的密码，错误的话就更新密码
                User userAD = iUserService.getUserByName(userName);
                String md5Pwd = Encryption.getMD5(passWord, userAD.getUid()).toString();
                String pwd2 = userAD.getPwd();
                if(!(md5Pwd.equals(pwd2))){
                    userAD.setPwd(md5Pwd);
                    int updatePwd = iUserService.updateByPrimaryKeySelective(userAD);
                    if(updatePwd>0){
                        System.out.println("已更新密码保存进数据库！");
                    }else {
                        System.out.println("更新密码失败！");
                    }
                }
            }
        }else{
            //验证失败
            System.out.println("域登录验证失败！");
        }


        //1.验证码校验
//        if (StringUtils.isEmpty(inputCode))
//        {
//            throw new Exception("验证码为空！");
//        }
//
//        if (!inputCode.equals(code.toUpperCase()))
//        {
//            throw new Exception("验证码错误！");
//        }


        //用户名或者密码为空
        if (StringUtils.isEmpty(loginName)||StringUtils.isEmpty(pwd))
        {
            throw new Exception("用户或密码为空！</br>The user or password is null!");
        }
    }

}

package com.yj.oa.project.controller;

import com.yj.oa.framework.web.controller.BaseController;
import com.yj.oa.framework.web.page.TableDataInfo;
import com.yj.oa.project.po.HrUser;
import com.yj.oa.project.service.hrUser.IHrUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
  Created by kelvavn.cai on 2019/7/2 0002.
 */
@Controller
@RequestMapping("/hrUser")
public class HrUserController extends BaseController{

    private String prefix = "system/hrUser/";

    @Autowired
    IHrUserService iHrUserService;

    /**
     * @描述 HR用户数据
     * @date 2018/9/15 12:30
     */
    @RequestMapping("/tableList2")
    @ResponseBody
    public TableDataInfo list(HrUser hrUser){
        startPage();
        List<HrUser> hrUsers = iHrUserService.selectByHrUser(hrUser);
        return getDataTable(hrUsers);
    }


}

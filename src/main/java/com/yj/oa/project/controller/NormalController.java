package com.yj.oa.project.controller;

import com.yj.oa.common.base.AjaxResult2;
import com.yj.oa.common.utils.file.FileUtil;
import com.yj.oa.common.utils.file.UploadFile;
import com.yj.oa.common.utils.poi.ExcelUtil;
import com.yj.oa.framework.web.controller.BaseController;
import com.yj.oa.framework.web.page.TableDataInfo;
import com.yj.oa.framework.web.po.AjaxResult;
import com.yj.oa.project.po.Normal;
import com.yj.oa.project.service.normal.INormalService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
  Created by kelvan.cai on 2019/7/8 0008.
 */
@Controller
@RequestMapping("/normal")
public class NormalController extends BaseController{
    private Logger Log= LoggerFactory.getLogger(this.getClass());
    private String prefix = "system/normal/";

    @Autowired
    INormalService iNormalService;

    /**
     * 导出
     * @param normal
     * @return
     */
    @RequestMapping("/export")
    @RequiresPermissions("normal:export")
    @ResponseBody
    public AjaxResult2 export(Normal normal){
        List<Normal> list = iNormalService.selectNormalList(normal);
        ExcelUtil<Normal> util = new ExcelUtil<Normal>(Normal.class);
        return util.exportExcel(list, "文件导出");
    }

    /**
     *
     * @描述 页面跳转
     *
     * @date 2018/9/16 10:59
     */
    @RequestMapping("/tolist")
    @RequiresPermissions("normal:list")
    public String tolist()
    {
        return prefix + "normal";
    }

    /**
     *
     * @描述 表格列表
     *
     * @date 2018/9/16 10:52
     */
    @RequestMapping("/tableList")
    @ResponseBody
    public TableDataInfo listPag(Normal normal)
    {
        startPage();
        List<Normal> normals = iNormalService.selectNormalList(normal);
        return getDataTable(normals);
    }

    /**
     *
     * @描述 新增页面
     *
     * @date 2018/9/16 11:37
     */

    @RequestMapping("/toAdd")
    @RequiresPermissions("normal:upload")
    public String toAdd()
    {
        return prefix + "add";
    }

    /**
     *
     * @描述 上传文件
     *
     * @date 2018/9/19 13:00
     */
    @RequestMapping("/addSave")
    @RequiresPermissions("normal:upload")
    @ResponseBody
    public AjaxResult addSave(MultipartFile file, Normal fileBean)
    {
//        if (file.isEmpty())
//        {
//            return error("请选择文件！");
//        }
//
//        // 上传
//        String fileId = null;
//        try
//        {
//            fileId = UploadFile.upload(file);
//        }
//        catch (IOException e)
//        {
//            return error();
//        }
//        if (fileId == null)
//        {
//            return error("上传失败！请再试试！");
//        }
//
//        iNormalService.insertNormal(getFileBean(file, fileId, fileBean,getUser().getName()));

        return success();
    }

    /**
     *
     * @描述 批量删除
     *
     * @date 2018/9/16 11:53
     */
    @RequestMapping("/del")
    @RequiresPermissions("normal:del")
    @ResponseBody
    public AjaxResult del(String[] ids)
    {

        // 1.删除前需要判断是否是本人发布的公告或这通知


        //2.删除数据库
        int i = iNormalService.deleteByPrimaryKeys(ids);

        if (i > 0)
        {
            // 3.删除服务器上的文件
            try
            {
                UploadFile.delFile(ids);
            }
            catch (IOException e)
            {
                Log.error("文件已从数据库删除,删除服务器文件出现异常", e.toString());
            }
        }
        return success();
    }

    /**
     *
     * @描述 文件下载
     *
     * @date 2018/9/19 12:09
     */
    @RequestMapping("/download")
    @RequiresPermissions("normal:download")
    public ResponseEntity<byte[]> download(String fileId, String fileName)
    {
        try
        {
            Normal normal = new Normal();
//            normal.setId(fileId);
            return UploadFile.download(fileId, fileName);
        }
        catch (IOException e)
        {
            return null;
        }
    }

//    @RequiresPermissions("system:normal:list")
//    @PostMapping("/list")
//    @ResponseBody
//    public TableDataInfo list(Normal normal)
//    {
//        startPage();
//        List<Normal> list = iNormalService.selectNormalList(normal);
//        return getDataTable(list);
//    }
//
//    @RequiresPermissions("system:normal:import")
//    @PostMapping("/importData")
//    @ResponseBody
//    public AjaxResult2 importData(MultipartFile file, boolean updateSupport) throws Exception
//    {
//        ExcelUtil<Normal> util = new ExcelUtil<Normal>(Normal.class);
//        List<Normal> normalList = util.importExcel(file.getInputStream());
////        String operName = getUser().getLoginName();
//
//        String message = iNormalService.importNormal(normalList, updateSupport);
//        return AjaxResult2.success(message);
//    }
//
//    @RequiresPermissions("system:normal:view")
//    @GetMapping("/importTemplate")
//    @ResponseBody
//    public AjaxResult2 importTemplate()
//    {
//        ExcelUtil<Normal> util = new ExcelUtil<Normal>(Normal.class);
//        return util.importTemplateExcel("工人数据");
//    }

    /**
     *
     * @描述 上传类 封装Files Bean
     *
     * @date 2018/9/19 15:20
     */
    public static Normal getFileBean(MultipartFile file, String fileId, Normal normalBean,String userName)
    {
        normalBean.setCreateTime(new Date());
        normalBean.setFileName(file.getOriginalFilename());
//        normalBean.setId(fileId);
        normalBean.setFileType(FileUtil.getType(file.getContentType()));
//        登录人
        normalBean.setUploadBy(userName);

//        将字节单位转换为MB
        normalBean.setFileSize(FileUtil.getFileSize(file.getSize()));
        return normalBean;
    }
}

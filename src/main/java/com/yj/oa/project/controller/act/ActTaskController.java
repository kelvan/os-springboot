package com.yj.oa.project.controller.act;

import com.yj.oa.common.utils.file.FileUploadUtils;
import com.yj.oa.common.utils.file.FileUtil;
import com.yj.oa.common.utils.poi.ExcelReaderUtil;
import com.yj.oa.framework.annotation.Operlog;
import com.yj.oa.framework.web.controller.BaseController;
import com.yj.oa.framework.web.page.TableDataInfo;
import com.yj.oa.framework.web.po.AjaxResult;
import com.yj.oa.project.po.*;
import com.yj.oa.project.service.ACT.applyRoom.IActApplyRoomFormService;
import com.yj.oa.project.service.ACT.task.IActTaskService;
import com.yj.oa.project.service.file.IFileService;
import com.yj.oa.project.service.leavForm.ILeavFormService;
import com.yj.oa.project.service.normal.INormalService;
import com.yj.oa.project.service.user.IUserService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author kelvan.cai
 */
@Controller
@RequestMapping("/task")
public class ActTaskController extends BaseController{

    private String prefix = "system/actTask/";

    @Autowired
    IActTaskService iacttaskService;

    @Autowired
    ILeavFormService iLeavFormService;
    @Autowired
    IUserService iUserService;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    HistoryService historyService;

    @Autowired
    TaskService taskService;

    @Autowired
    IActApplyRoomFormService iActApplyRoomFormService;

    @Autowired
    IActApplyRoomFormService iact_applyRoomFormService;

    @Autowired
    IFileService iFileService;

    @Autowired
    INormalService iNormalService;


    /**
     *
     * @描述 跳转到任务列表页面
     *
     * @date 2018/9/22 11:00
     */
    @RequestMapping("/toMyTaskList")
    public String toTask()
    {
        return prefix + "actTask";
    }


    /**
     *
     * @描述: 任务表格数据
     *
     * @params:
     * @return:
     * @date: 2018/9/24 17:26
     */
    @RequestMapping("/TableMyTasklList")
    @ResponseBody
    public TableDataInfo taskList(ActTask actTask)
    {
        startPage(); //20180909000000_111  20180914-2
        actTask.setAssignee(getUserId());
        List<ActTask> actTasks = iacttaskService.selectACTTaskList(actTask);
        for (ActTask task : actTasks)
        {
            User user = iUserService.selectByPrimaryKey(task.getAssignee());
            task.setAssignee(user.getName());
        }
        return getDataTable(actTasks);
    }


    /**
     *
     * @描述 查看申请表单内容
     *
     * @date 2018/9/22 12:48
     */
    @RequestMapping("/edit/{formKey}/{procInstId}/{taskId}")
    @Operlog(modal = "流程审批",descr = "查看待审表单信息")
    public String edit(@PathVariable("formKey") String formId,
                       @PathVariable("taskId") String taskId,
                       @PathVariable("procInstId") String procInstId,
                       Model model)
    {

        //判断任务审批是申请类型 还是 会议室申请 通过 BusinessKey来判断
        HistoricProcessInstance result = historyService.createHistoricProcessInstanceQuery().
                processInstanceId(procInstId).singleResult();

        String key = result.getBusinessKey();

        if (key.indexOf("0") == 0)
        {
            //会议室申请
            ApplyRoomForm applyRoomForm = iActApplyRoomFormService.selectByPrimaryKey(formId);
            User user = iUserService.selectByPrimaryKey(applyRoomForm.getProposer_Id());
            applyRoomForm.setProposer_Id(user.getName());
            model.addAttribute("Form", applyRoomForm);
            model.addAttribute("taskId", taskId);
            return prefix + "editApplyRoomForm";
        }

        //借人申请
        LeaveForm leaveForm = iLeavFormService.selectByPrimaryKey(Integer.valueOf(formId));
        System.out.println(leaveForm);
        User user = iUserService.selectByPrimaryKey(leaveForm.getProposer_Id());
        leaveForm.setProposer_Id(user.getName());
        List<User> users = iUserService.selectByUser(new User());
        List<User> approver = iUserService.getNextApprover(new User());//获取下一个审批人（默认为Ann.Zhuo）
        model.addAttribute("Form", leaveForm);
        model.addAttribute("taskId", taskId);
        model.addAttribute("users", users);
        model.addAttribute("approver",approver);
        if(leaveForm.getStatus()==2){
            return prefix + "editLeaveForm2";
        }
        return prefix + "editLeaveForm";
    }


    /**
     *
     * @描述 compelete 会议申请 审批
     *
     * @date 2018/9/22 12:52
     */
    @RequestMapping("/RoomApproval")
    @ResponseBody
    @Operlog(modal = "流程审批",descr = "会议室审批")
    public AjaxResult approval(ApplyRoomForm applyRoomForm, String taskId)
    {
        try
        {
            iacttaskService.RoomApproval(applyRoomForm, taskId);
        }
        catch (Exception e)
        {
            return error(e.getMessage());
        }
        return success();
    }


    /**
     *
     * @描述 compelete 借人申请 审批
     *
     * @date 2018/9/22 12:52
     */
    @RequestMapping("/LeaveApproval")
    @Operlog(modal = "流程审批",descr = "申请审批")
    @ResponseBody
    public AjaxResult LeaveApproval(LeaveForm leaveForm, String taskId)
    {

        try
        {
            iacttaskService.LeaveApproval(leaveForm, taskId);
        }
        catch (Exception e)
        {
            return error(e.getMessage());
        }
        return success();
    }


    /**
     *
     * @描述: 删除待审任务
     *
     * @params:
     * @return:
     * @date: 2018/9/26 15:01
     */
    @RequestMapping("/del")
    @Operlog(modal = "流程审批",descr = "删除待审任务")
    @ResponseBody
    public AjaxResult del(String[] ids)
    {
        return result(iacttaskService.deletByProcInstS(ids));
    }


//    /**
//     *
//     * @描述 上传文件
//     *
//     * @date 2018/9/19 13:00
//     */
//    @RequestMapping("/addSave")
//    @RequiresPermissions("file:upload")
//    @Operlog(modal = "文件管理",descr = "上传文件")
//    @ResponseBody
//    public AjaxResult addSave(MultipartFile file, Files fileBean)
//    {
//        if (file.isEmpty())
//        {
//            return error("请选择文件！");
//        }
//
//        // 上传
//        String fileId = null;
//        try
//        {
//            fileId = FileUploadUtils.upload(file);
//
//        }
//        catch (IOException e)
//        {
//            return error();
//        }
//        //读取Excel保存进数据库
//        String path = "C:/Users/Administrator/Desktop/1562206021338用户数据.xlsx";
//        List<List<String>> lists = ExcelReaderUtil.readExcel(path);
//        Normal normal  = new Normal();
//        for (List<String> list : lists) {
//            for (String strs : list) {
//                System.out.println("Excel内容："+strs);
//
//            }
//            System.out.println("ExcelList内容"+list);
////            normal.setId(list.get(0));
//            normal.setEmployeeNo(list.get(1));
//            normal.setChnName(list.get(2));
//            iNormalService.insertNormal(normal);
//        }
//        if (fileId == null)
//        {
//            return error("上传失败！请再试试！");
//        }
//
//        iFileService.insertSelective(getFileBean(file, fileId, fileBean,getUser().getName()));
//
//        return success();
//    }
//
//    /**
//     *
//     * @描述 上传类 封装Files Bean
//     *
//     * @date 2018/9/19 15:20
//     */
//    public static Files getFileBean(MultipartFile file, String fileId, Files fileBean, String userName)
//    {
//        fileBean.setCreateTime(new Date());
//        fileBean.setFileName(file.getOriginalFilename());
//        fileBean.setFileId(fileId);
//        fileBean.setFileType(FileUtil.getType(file.getContentType()));
////        登录人
//        fileBean.setUploadUser(userName);
//
////        将字节单位转换为MB
//        fileBean.setFileSize(FileUtil.getFileSize(file.getSize()));
//        return fileBean;
//    }

}

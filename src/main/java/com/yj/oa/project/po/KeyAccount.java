package com.yj.oa.project.po;

import com.yj.oa.framework.web.po.BasePo;

import java.util.Date;

/**
  Created by kelvan.cai on 2019/7/10 0010.
 */
public class KeyAccount extends BasePo{

    //id int
    //keyAccount nvarchar
    //status int
    //create by narchar
    //createDate Date

    //id
    private Integer id;

    //主要客户
    private String keyAccount;

    //状态
    private Integer status;

    //创建人
    private String createBy;

    //创建时间
    private Date createDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyAccount() {
        return keyAccount;
    }

    public void setKeyAccount(String keyAccount) {
        this.keyAccount = keyAccount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}

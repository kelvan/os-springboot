package com.yj.oa.project.po;

import com.yj.oa.framework.web.po.BasePo;

import javax.xml.crypto.Data;
import java.util.Date;

/**
 Created by kelvan.cai on 2019/7/10 0010.
 */
public class ProjectInfo extends BasePo{
    //id int
    //kcID int
    //projectCode varchar
    //itemCode nvarchar
    //projectName narchar
    //targetPassRate numeric
    //firstPassRate numeric
    //status int
    //createBy nvarchar
    //createDate datetime TIMESTAMP

    //id
    private Integer id;

    //kcID 客户ID
    private Integer kcID;

    //项目编号
    private String projectCode;

    //项目编码
    private String itemCode;

    //项目名
    private String projectName;

    //状态
    private Integer status;

    //创建人
    private String createBy;

    //创建时间
    private String createDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getKcID() {
        return kcID;
    }

    public void setKcID(Integer kcID) {
        this.kcID = kcID;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}

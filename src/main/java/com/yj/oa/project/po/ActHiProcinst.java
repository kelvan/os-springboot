package com.yj.oa.project.po;

import com.yj.oa.common.annotation.Excel;
import com.yj.oa.framework.web.po.BasePo;

import java.util.Date;

/**
 * @author kelvan.cai
 *
 * @描述  实例进程，一个申请一个实例
 *
 * @date 2018/9/25 14:22
 */
public class ActHiProcinst extends BasePo{

    @Excel(name = "编号")
    private String id;

    //英文名
    @Excel(name = "申请人")
    private String englishName;

    //上级领导英文名
    @Excel(name = "审批人")
    private String supervisorEnName;

    //部门
    @Excel(name = "部门")
    private String hrDept;

    //部门小组
    @Excel(name = "成本中心")
    private String costCenter;

    @Excel(name = "需求时间合计")
    private Long totalTime;

    @Excel(name = "客户")
    private String customer;

    @Excel(name = "支持项目")
    private String supportingProject;

    @Excel(name = "procInstId")
    private String procInstId;

    private String businessKey;

    private String procDefId;

    private Date startTime;

    private Date endTime;

    private Long duration;

    private String startUserId;

    private String startActId;

    private String endActId;

    private String superProcessInstanceId;

    private String deleteReason;

    private String tenantId;

    private String name;

    private User user;

    private String loginName;

    @Excel(name="requestedOne")
    private Integer requestedOne;

    private Integer requestedTwo;

    private LeaveForm leave;

    private Dept dept;


    @Excel(name="DeptName")
    private String deptName;

    @Excel(name="hr")
    private HrUser hrUser;

    //工号
    private String employeeNo;

    //中文名
    private String chnName;

    //性别
    private String gender;

    //工作邮箱
    private String workEmail;

    //手机号
    private Integer mobileNo;

    //职位中文名
    private String positionCnName;

    //职位中文名
    private String positionEnName;

    //上级领导工号
    private String supervisorNo;

    //上级领导中文名
    private String supervisorChnName;

    //上级领导邮箱
    private String supervisorMail;

    //上级领导职位
    private String supervisorPosition;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getProcInstId() {
        return procInstId;
    }

    public void setProcInstId(String procInstId) {
        this.procInstId = procInstId == null ? null : procInstId.trim();
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey == null ? null : businessKey.trim();
    }

    public String getProcDefId() {
        return procDefId;
    }

    public void setProcDefId(String procDefId) {
        this.procDefId = procDefId == null ? null : procDefId.trim();
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getStartUserId() {
        return startUserId;
    }

    public void setStartUserId(String startUserId) {
        this.startUserId = startUserId == null ? null : startUserId.trim();
    }

    public String getStartActId() {
        return startActId;
    }

    public void setStartActId(String startActId) {
        this.startActId = startActId == null ? null : startActId.trim();
    }

    public String getEndActId() {
        return endActId;
    }

    public void setEndActId(String endActId) {
        this.endActId = endActId == null ? null : endActId.trim();
    }

    public String getSuperProcessInstanceId() {
        return superProcessInstanceId;
    }

    public void setSuperProcessInstanceId(String superProcessInstanceId) {
        this.superProcessInstanceId = superProcessInstanceId == null ? null : superProcessInstanceId.trim();
    }

    public String getDeleteReason() {
        return deleteReason;
    }

    public void setDeleteReason(String deleteReason) {
        this.deleteReason = deleteReason == null ? null : deleteReason.trim();
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId == null ? null : tenantId.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getRequestedOne() {
        return requestedOne;
    }

    public void setRequestedOne(Integer requestedOne) {
        this.requestedOne = requestedOne;
    }

    public Integer getRequestedTwo() {
        return requestedTwo;
    }

    public void setRequestedTwo(Integer requestedTwo) {
        this.requestedTwo = requestedTwo;
    }

    public String getSupportingProject() {
        return supportingProject;
    }

    public void setSupportingProject(String supportingProject) {
        this.supportingProject = supportingProject;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Long totalTime) {
        this.totalTime = totalTime;
    }

    public LeaveForm getLeave() {
        return leave;
    }

    public void setLeave(LeaveForm leave) {
        this.leave = leave;
    }

    public Dept getDept() {
        return dept;
    }

    public void setDept(Dept dept) {
        this.dept = dept;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public HrUser getHrUser() {
        return hrUser;
    }

    public void setHrUser(HrUser hrUser) {
        this.hrUser = hrUser;
    }

    public String getWorkEmail() {
        return workEmail;
    }

    public void setWorkEmail(String workEmail) {
        this.workEmail = workEmail;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getEmployeeNo() {
        return employeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    public String getChnName() {
        return chnName;
    }

    public void setChnName(String chnName) {
        this.chnName = chnName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(Integer mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getHrDept() {
        return hrDept;
    }

    public void setHrDept(String hrDept) {
        this.hrDept = hrDept;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public String getPositionCnName() {
        return positionCnName;
    }

    public void setPositionCnName(String positionCnName) {
        this.positionCnName = positionCnName;
    }

    public String getPositionEnName() {
        return positionEnName;
    }

    public void setPositionEnName(String positionEnName) {
        this.positionEnName = positionEnName;
    }

    public String getSupervisorNo() {
        return supervisorNo;
    }

    public void setSupervisorNo(String supervisorNo) {
        this.supervisorNo = supervisorNo;
    }

    public String getSupervisorChnName() {
        return supervisorChnName;
    }

    public void setSupervisorChnName(String supervisorChnName) {
        this.supervisorChnName = supervisorChnName;
    }

    public String getSupervisorEnName() {
        return supervisorEnName;
    }

    public void setSupervisorEnName(String supervisorEnName) {
        this.supervisorEnName = supervisorEnName;
    }

    public String getSupervisorMail() {
        return supervisorMail;
    }

    public void setSupervisorMail(String supervisorMail) {
        this.supervisorMail = supervisorMail;
    }

    public String getSupervisorPosition() {
        return supervisorPosition;
    }

    public void setSupervisorPosition(String supervisorPosition) {
        this.supervisorPosition = supervisorPosition;
    }

    @Override
    public String toString()
    {
        final StringBuffer sb = new StringBuffer("ActHiProcinst{");
        sb.append("id='").append(id).append('\'');
        sb.append(", procInstId='").append(procInstId).append('\'');
        sb.append(", businessKey='").append(businessKey).append('\'');
        sb.append(", procDefId='").append(procDefId).append('\'');
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", duration=").append(duration);
        sb.append(", startUserId='").append(startUserId).append('\'');
        sb.append(", startActId='").append(startActId).append('\'');
        sb.append(", endActId='").append(endActId).append('\'');
        sb.append(", superProcessInstanceId='").append(superProcessInstanceId).append('\'');
        sb.append(", deleteReason='").append(deleteReason).append('\'');
        sb.append(", tenantId='").append(tenantId).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", requestedOne=").append(requestedOne);
        sb.append(", requestedTwo=").append(requestedTwo);
        sb.append(", supportingProject=").append(supportingProject).append('\'');
        sb.append(", customer=").append(customer).append('\'');
        sb.append(", totalTime=").append(totalTime);
        sb.append(", deptName=").append(deptName).append('\'');
        sb.append(", employeeNo=").append(employeeNo).append('\'');
        sb.append(", chnName=").append(chnName).append('\'');
        sb.append(", englishName=").append(englishName).append('\'');
        sb.append(", gender=").append(gender).append('\'');
        sb.append(", workEmail=").append(workEmail).append('\'');
        sb.append(", mobileNo=").append(mobileNo);
        sb.append(", hrDept=").append(hrDept).append('\'');
        sb.append(", costCenter=").append(costCenter).append('\'');
        sb.append(", positionCnName=").append(positionCnName).append('\'');
        sb.append(", positionEnName=").append(positionEnName).append('\'');
        sb.append(", supervisorNo=").append(supervisorNo).append('\'');
        sb.append(", supervisorChnName=").append(supervisorChnName).append('\'');
        sb.append(", supervisorEnName=").append(supervisorEnName).append('\'');
        sb.append(", supervisorMail=").append(supervisorMail).append('\'');
        sb.append(", supervisorPosition=").append(supervisorPosition).append('\'');
        sb.append(", loginName=").append(loginName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
package com.yj.oa.project.po;

import com.yj.oa.framework.web.po.BasePo;


/**
  Created by kelvan.cai on 2019/6/28 0028.
 */
public class HrUser extends BasePo {

    //工号
    private String employeeNo;

    //中文名
    private String chnName;

    //英文名
    private String englishName;

    //性别
    private String gender;

    //工作邮箱
    private String workEmail;

    //手机号
    private String mobileNo;

    //部门
    private String hrDept;

    //部门小组
    private String costCenter;

    //职位中文名
    private String positionCnName;

    //职位中文名
    private String positionEnName;

    //上级领导工号
    private String supervisorNo;

    //上级领导中文名
    private String supervisorChnName;

    //上级领导英文名
    private String supervisorEnName;

    //上级领导邮箱
    private String supervisorMail;

    //上级领导职位
    private String supervisorPosition;


    public String getEmployeeNo() {
        return employeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    public String getChnName() {
        return chnName;
    }

    public void setChnName(String chnName) {
        this.chnName = chnName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getWorkEmail() {
        return workEmail;
    }

    public void setWorkEmail(String workEmail) {
        this.workEmail = workEmail;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getHrDept() {
        return hrDept;
    }

    public void setHrDept(String hrDept) {
        this.hrDept = hrDept;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public String getPositionCnName() {
        return positionCnName;
    }

    public void setPositionCnName(String positionCnName) {
        this.positionCnName = positionCnName;
    }

    public String getPositionEnName() {
        return positionEnName;
    }

    public void setPositionEnName(String positionEnName) {
        this.positionEnName = positionEnName;
    }

    public String getSupervisorNo() {
        return supervisorNo;
    }

    public void setSupervisorNo(String supervisorNo) {
        this.supervisorNo = supervisorNo;
    }

    public String getSupervisorChnName() {
        return supervisorChnName;
    }

    public void setSupervisorChnName(String supervisorChnName) {
        this.supervisorChnName = supervisorChnName;
    }

    public String getSupervisorEnName() {
        return supervisorEnName;
    }

    public void setSupervisorEnName(String supervisorEnName) {
        this.supervisorEnName = supervisorEnName;
    }

    public String getSupervisorMail() {
        return supervisorMail;
    }

    public void setSupervisorMail(String supervisorMail) {
        this.supervisorMail = supervisorMail;
    }

    public String getSupervisorPosition() {
        return supervisorPosition;
    }

    public void setSupervisorPosition(String supervisorPosition) {
        this.supervisorPosition = supervisorPosition;
    }

    @Override
    public String toString()
    {
        final StringBuffer sb = new StringBuffer("HrUser{");
        sb.append("employeeNo='").append(employeeNo).append('\'');
        sb.append(", chnName='").append(chnName).append('\'');
        sb.append(", englishName='").append(englishName).append('\'');
        sb.append(", gender='").append(gender).append('\'');
        sb.append(", workEmail='").append(workEmail).append('\'');
        sb.append(", mobileNo=").append(mobileNo);
        sb.append(", hrDept='").append(hrDept).append('\'');
        sb.append(", costCenter='").append(costCenter).append('\'');
        sb.append(", positionCnName='").append(positionCnName).append('\'');
        sb.append(", positionEnName='").append(positionEnName).append('\'');
        sb.append(", supervisorNo='").append(supervisorNo).append('\'');
        sb.append(", supervisorChnName='").append(supervisorChnName).append('\'');
        sb.append(", supervisorEnName='").append(supervisorEnName).append('\'');
        sb.append(", supervisorMail='").append(supervisorMail).append('\'');
        sb.append(", supervisorPosition='").append(supervisorPosition).append('\'');
        sb.append("}");
        return sb.toString();
    }
}

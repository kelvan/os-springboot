package com.yj.oa.project.po;

import com.yj.oa.common.annotation.Excel;
import com.yj.oa.framework.web.po.BasePo;

import java.util.Date;

/**
  Created by kelvan.cai on 2019/7/5 0005.
 */
public class Normal extends BasePo {

    /**
     * 工号employee_no
     中文姓名chn_name
     工种position_cn_name
     部门dept
     生产线line
     组长supervisor_chn_name
     创建时间create_time
     上传者upload_by
     文件名file_name
     文件类型file_type
     文件大小file_size
     关联文件file_id
     */

    //关联名单文件file_id
    private Integer fileId;

    //自增id
    @Excel(name = "编号")
    private Integer id;

    //工号employee_no
    @Excel(name = "工号")
    private String employeeNo;

    //中文姓名chn_name
    @Excel(name = "中文姓名")
    private String chnName;

    //工种position_cn_name
    @Excel(name = "工种")
    private String positionCnName;

    //部门dept
    @Excel(name = "部门")
    private String dept;

    //生产线line
    @Excel(name = "生产线")
    private String line;

    //组长supervisor_chn_name
    @Excel(name = "组长")
    private String supervisorChnName;

    //创建时间create_time
    @Excel(name = "创建时间")
    private Date createTime;

    //上传者upload_by
    @Excel(name = "上传者")
    private String uploadBy;

    //文件名fileName
    @Excel(name = "文件名")
    private String fileName;
    //文件类型fileType
    @Excel(name = "文件类型")
    private String fileType;
    //文件大小fileSize
    @Excel(name = "文件大小")
    private String fileSize;

    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmployeeNo() {
        return employeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    public String getChnName() {
        return chnName;
    }

    public void setChnName(String chnName) {
        this.chnName = chnName;
    }

    public String getPositionCnName() {
        return positionCnName;
    }

    public void setPositionCnName(String positionCnName) {
        this.positionCnName = positionCnName;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getSupervisorChnName() {
        return supervisorChnName;
    }

    public void setSupervisorChnName(String supervisorChnName) {
        this.supervisorChnName = supervisorChnName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUploadBy() {
        return uploadBy;
    }

    public void setUploadBy(String uploadBy) {
        this.uploadBy = uploadBy;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Normal{");
        sb.append("fileId=").append(fileId);
        sb.append(", id=").append(id);
        sb.append(", employeeNo='").append(employeeNo).append('\'');
        sb.append(", chnName='").append(chnName).append('\'');
        sb.append(", positionCnName='").append(positionCnName).append('\'');
        sb.append(", dept='").append(dept).append('\'');
        sb.append(", line='").append(line).append('\'');
        sb.append(", supervisorChnName='").append(supervisorChnName).append('\'');
        sb.append(", createTime=").append(createTime);
        sb.append(", uploadBy='").append(uploadBy).append('\'');
        sb.append(", fileName='").append(fileName).append('\'');
        sb.append(", fileType='").append(fileType).append('\'');
        sb.append(", fileSize='").append(fileSize).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

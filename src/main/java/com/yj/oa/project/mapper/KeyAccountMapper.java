package com.yj.oa.project.mapper;

import com.yj.oa.project.po.KeyAccount;

import java.util.List;

/**
  Created by kelvan.cai on 2019/7/10 0010.
 */
public interface KeyAccountMapper {

    List<KeyAccount> selectKeyAccount();
}

package com.yj.oa.project.mapper;

import com.yj.oa.project.po.Normal;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
  Created by kelvan.cai on 2019/7/8 0008.
 */
public interface NormalMapper {

    /**
     * 列表
     * @param normal
     * @return
     */
    List<Normal> selectNormalList(Normal normal);

    /**
     * 主键查询
     * @param id
     * @return
     */
    Normal selectByPrimaryKey(Integer id);

    /**
     * 批量删除
     * @param id
     * @return
     */
    int deleteByPrimaryKeys(String[] id);

    /**
     * 新增工人信息
     * @param normal
     * @return
     */
    int insertNormal(Normal normal);



    /**
     * 修改工人信息
     * @param normal
     * @return
     */
    int updateNormal(Normal normal);

    /**
     * 通过工人ID删除工人
     * @param id
     * @return
     */
    int deleteNormalById(Long id);



    /**
     * 通过用户名查询用户
     *
     * @param chnName 工人名
     * @return 用户对象信息
     */
    Normal selectNormalBychnName(String chnName);

    /**
     * 通过申请表id关联过去上传文件的id，再获取实际上传的名单
     * @param leaveId
     * @return
     */
    @Select("SELECT l.id,n.* FROM t_normal n LEFT JOIN t_file f ON f.id = n.file_id LEFT JOIN t_leaveform l ON l.id = f.leaveId WHERE l.id = #{leaveId}")
    List<Normal> selectNormalByLeaveId(String leaveId);

}

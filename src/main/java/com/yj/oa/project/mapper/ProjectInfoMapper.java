package com.yj.oa.project.mapper;

import com.yj.oa.project.po.ProjectInfo;

import java.util.List;

/**
 Created by kelvan.cai on 2019/7/10 0010.
 */
public interface ProjectInfoMapper {

    List<ProjectInfo> selectProjectInfo();
}

package com.yj.oa.project.mapper;


import com.yj.oa.project.po.HrUser;

import java.util.List;

/**
  Created by kelvan.cai on 2019/7/1 0001.
 */
public interface HrUserMapper {

    /**
     * 根据用户名获取HR用户信息
     * @param userName
     * @return
     */
    HrUser getHrUserByName(String userName);


    /**
     *  HR用户列表 以及 根据条件查询
     * @param hrUser
     * @return
     */
    List<HrUser> selectByHrUser(HrUser hrUser);
}

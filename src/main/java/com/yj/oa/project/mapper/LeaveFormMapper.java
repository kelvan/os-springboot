package com.yj.oa.project.mapper;

import com.yj.oa.project.po.ApplyRoomForm;
import com.yj.oa.project.po.LeaveForm;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface LeaveFormMapper{
    /**
     *
     * 批量删除
     * @mbggenerated
     */
    int deleteByprocInstIds(String[] ids);

    /**
     *添加
     * @mbggenerated
     */
    int insertSelective(LeaveForm record);

    /**
     *主键查询
     *
     * @mbggenerated
     */
    LeaveForm selectByPrimaryKey(Integer id);

    /**
     * 根据用户名查找最新一条申请表信息
     * @param userName
     * @return
     */
    LeaveForm selectLeaveByUser(String userName);

    /**
     * 修改状态
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(LeaveForm record);


    /**
     * 列表
     * @mbggenerated
     */
    List<LeaveForm> selectLeavFormList(LeaveForm a);

    /**
     * 统计申请天数
     */
    List<LeaveForm> selectByUserIdAndDate(LeaveForm leaveForm);

    /**
     * 获取文件自增id
     * @param fileId
     * @return
     */
    @Select("SELECT f.id FROM t_leaveForm l LEFT JOIN t_file f ON f.leaveId = l.id where l.id = #{fileId}")
    int getFileId(Integer fileId);

    /**
     * 获取申请表的状态
     * @param leaveId
     * @return
     */
    @Select("SELECT l.status FROM t_leaveForm l LEFT JOIN t_file f ON f.leaveId = l.id WHERE l.id = #{leaveId}")
    int getStatus(Integer leaveId);
}
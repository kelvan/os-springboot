package com.yj.oa.project.service.normal;

import com.yj.oa.common.exception.BusinessException;
import com.yj.oa.common.utils.StringUtils;
import com.yj.oa.project.mapper.NormalMapper;
import com.yj.oa.project.po.Normal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
  Created by kelvan.cai on 2019/7/8 0008.
 */
@Service
@Transactional
public class NormalServiceImpl implements INormalService {

    private static final Logger log = LoggerFactory.getLogger(NormalServiceImpl.class);
    @Autowired
    private NormalMapper normalMapper;

    /**
     * 新增工人信息
     * @param normal
     * @return
     */
    @Override
    public int insertNormal(Normal normal) {
        return normalMapper.insertNormal(normal);
    }

    @Override
    public List<Normal> selectNormalByLeaveId(String leaveId) {
        return normalMapper.selectNormalByLeaveId(leaveId);
    }

    /**
     * 修改工人信息
     * @param normal
     * @return
     */
    @Override
    public int updateNormal(Normal normal) {
        return normalMapper.updateNormal(normal);
    }


    /**
     * 通过工人ID删除工人
     * @param id
     * @return
     */
    @Override
    public int deleteNormalById(Long id) {
        return normalMapper.deleteNormalById(id);
    }

    /**
     * 通过用户名查询用户
     *
     * @param chnName 工人名
     * @return 用户对象信息
     */
    @Override
    public Normal selectNormalBychnName(String chnName) {
        return normalMapper.selectNormalBychnName(chnName);
    }

    /**
     * 列表
     * @param normal
     * @return
     */
    @Override
    public List<Normal> selectNormalList(Normal normal) {
        return normalMapper.selectNormalList(normal);
    }

    /**
     * 主键查询
     * @param id
     * @return
     */
    @Override
    public Normal selectByPrimaryKey(Integer id) {
        return normalMapper.selectByPrimaryKey(id);
    }

    /**
     * 批量删除
     * @param id
     * @return
     */
    @Override
    public int deleteByPrimaryKeys(String[] id) {
        try{
            return normalMapper.deleteByPrimaryKeys(id);
        }catch (Exception e){
            log.error("$$$$$ 删除文件失败=[{}]",e);
            throw new RuntimeException("操作失败！");
        }
    }

    /**
     * 导入用户数据
     * @param normalList 工人数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @return 结果
     */
//    @Override
//    public String importNormal(List<Normal> normalList, Boolean isUpdateSupport) {
//        if (StringUtils.isNull(normalList) || normalList.size() == 0)
//        {
//            throw new BusinessException("导入工人数据不能为空！");
//        }
//        int successNum = 0;
//        int failureNum = 0;
//        StringBuilder successMsg = new StringBuilder();
//        StringBuilder failureMsg = new StringBuilder();
//        for (Normal normal : normalList)
//        {
//            try
//            {
//                // 验证是否存在这个工人
//                Normal u = normalMapper.selectNormalBychnName(normal.getChnName());
//                if (StringUtils.isNull(u))
//                {
//                    this.insertNormal(normal);
//                    successNum++;
//                    successMsg.append("<br/>" + successNum + "、工人 " + normal.getChnName() + " 导入成功");
//                }
//                else if (isUpdateSupport)
//                {
//                    this.updateNormal(normal);
//                    successNum++;
//                    successMsg.append("<br/>" + successNum + "、工人 " + normal.getChnName() + " 更新成功");
//                }
//                else
//                {
//                    failureNum++;
//                    failureMsg.append("<br/>" + failureNum + "、账号 " + normal.getChnName() + " 已存在");
//                }
//            }
//            catch (Exception e)
//            {
//                failureNum++;
//                String msg = "<br/>" + failureNum + "、账号 " + normal.getChnName() + " 导入失败：";
//                failureMsg.append(msg + e.getMessage());
//                log.error(msg, e);
//            }
//        }
//        if (failureNum > 0)
//        {
//            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
//            throw new BusinessException(failureMsg.toString());
//        }
//        else
//        {
//            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
//        }
//        return successMsg.toString();
//    }
}

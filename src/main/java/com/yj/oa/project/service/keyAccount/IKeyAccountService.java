package com.yj.oa.project.service.keyAccount;


import com.yj.oa.project.po.KeyAccount;

import java.util.List;

/**
 Created by kelvan.cai on 2019/7/10 0010.
 */
public interface IKeyAccountService {

    /**
     * keyAccount列表
     * @return
     */
    List<KeyAccount> selectKeyAccount();
}

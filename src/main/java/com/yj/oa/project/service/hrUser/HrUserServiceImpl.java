package com.yj.oa.project.service.hrUser;

import com.yj.oa.project.mapper.HrUserMapper;
import com.yj.oa.project.po.HrUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
  Created by kelvan.cai on 2019/7/2 0002.
 */
@Service
@Transactional
public class HrUserServiceImpl implements IHrUserService{

    @Autowired
    HrUserMapper hrUserMapper;

    @Override
    public HrUser getHrUserByName(String userName) {
        return hrUserMapper.getHrUserByName(userName);
    }

    @Override
    public List<HrUser> selectByHrUser(HrUser hrUser) {
        return hrUserMapper.selectByHrUser(hrUser);
    }
}

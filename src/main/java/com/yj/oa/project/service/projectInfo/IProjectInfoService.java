package com.yj.oa.project.service.projectInfo;

import com.yj.oa.project.po.ProjectInfo;

import java.util.List;

/**
 Created by kelvan.cai on 2019/7/10 0010.
 */
public interface IProjectInfoService {

    List<ProjectInfo> selectProjectInfo();
}

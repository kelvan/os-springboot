package com.yj.oa.project.service.projectInfo;

import com.yj.oa.project.mapper.ProjectInfoMapper;
import com.yj.oa.project.po.ProjectInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 Created by kelvan.cai on 2019/7/10 0010.
 */
@Service
public class ProjectInfoServiceImpl implements IProjectInfoService{
    @Autowired
    ProjectInfoMapper projectInfoMapper;

    @Override
    public List<ProjectInfo> selectProjectInfo() {
        return projectInfoMapper.selectProjectInfo();
    }
}

package com.yj.oa.project.service.keyAccount;

import com.yj.oa.project.mapper.KeyAccountMapper;
import com.yj.oa.project.po.KeyAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 Created by kelvan.cai on 2019/7/10 0010.
 */
@Service
public class KeyAccountServiceImpl implements IKeyAccountService{
    @Autowired
    KeyAccountMapper keyAccountMapper;

    @Override
    public List<KeyAccount> selectKeyAccount() {
        return keyAccountMapper.selectKeyAccount();
    }
}
